fruits = {
    bananas: 100,
    apples: 150,
    pears: 75
}
vegetables = {
    tomatoes: 65,
    cucumber: 120,
    onion: 30
}
sweets = {
    candies: 55,
    cookies: 250,
    chocolate: 175
}
obj = new Object()
products = [fruits, vegetables, sweets]
for(i=0;i<products.length;i++){
    for(p in products[i])
    {
        obj[p] = products[i][p]
    }
}
let sortable = [];
for (j in obj) {
    sortable.push([j, obj[j]]);
}
sortable.sort(function(a, b) {
      return a[1] - b[1];
 });
a = ['', 0]
for(i=0;i<sortable.length;i++){
    if(sortable[i][1]>a[1]){
        a[0] = sortable[i][0]
        a[1] = sortable[i][1]
    }
}
console.log('Самый дорогой товар -',a[0], ', цена которого равна -', a[1])
console.log('Отсортированные товары в порядке возрастания цены:', sortable)